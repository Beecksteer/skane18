package com.example.rebeckagerdtham.skane18.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rebeckagerdtham.skane18.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;


public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    RadioButton mtaker, mprovider;
    HashMap<String, String> userMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Toolbar modifications
        Toolbar my_toolbar = findViewById(R.id.mCustomToolbar);
        setSupportActionBar(my_toolbar);
        getSupportActionBar().setTitle("Skapa nytt konto");
        getSupportActionBar().setLogo(R.drawable.log);

        mAuth = FirebaseAuth.getInstance();
        final EditText mEmail = findViewById(R.id.insertemail);
        final EditText mPass = findViewById(R.id.insertpassword);
        final EditText mName = findViewById(R.id.insertname);

        Button registerbutton = findViewById(R.id.createNewAccountButton);
        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = mEmail.getEditableText().toString();
                String password = mPass.getEditableText().toString();
                String name = mName.getEditableText().toString();

                register_user(email, password, name);

            }
        });

        //Lägger till användarens status, Caretaker eller Careprovider, i userMap beroende på vilken radiobutton som klickas i.
        mtaker = findViewById(R.id.radioButtonTaker);
        mtaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mtaker.isChecked()){
                    userMap.put("status", "Caretaker");
                }
            }
        });
        mprovider = findViewById(R.id.radioButtonProvider);
        mprovider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mprovider.isChecked()){
                    userMap.put("status", "Careprovider");
                }
            }
        });


    }

    private void register_user(String email, final String password, final String name) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {

                    //Lägger till användaren i databasen under child "users", lägger dessutom namnet i child "name".
                    FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                    String uid = current_user.getUid();
                    mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(uid);


                    userMap.put("name", name);


                    mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Intent menuIntent = new Intent (RegisterActivity.this,menuActivity.class);
                                startActivity(menuIntent);
                                Toast.makeText(RegisterActivity.this, "Registeringen lyckades", Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }
                    });
                } else {
                    Toast.makeText(RegisterActivity.this, "Registeringen misslyckades", Toast.LENGTH_LONG).show();
                }

            }
        });

    }




}

