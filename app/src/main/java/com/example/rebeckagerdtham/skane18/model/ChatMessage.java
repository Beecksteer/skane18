package com.example.rebeckagerdtham.skane18.model;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by RebeckaGerdtham on 2018-02-05.
 */

public class ChatMessage {
    private String date;
    private String ownerId;
    private String message;

    public ChatMessage(String ownerId, String message){
            this.setDate("dagens datum");
            this.ownerId= ownerId;
            this.message= message;

    }
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
