package com.example.rebeckagerdtham.skane18.model;

import java.util.List;

/**
 * Created by RebeckaGerdtham on 2018-02-05.
 */

public class Patient {
    private String patientId;
    private String fullname;
    private String password;
    private List<Chat> chats;

    public List<Chat> getChats() {
        return chats;
    }

    public void setChats(List<Chat> chats) {
        this.chats = chats;
    }

//generator + getter och setter

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPatientPassword(String password) {
        this.password = password;
    }



}
