package com.example.rebeckagerdtham.skane18.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rebeckagerdtham.skane18.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SettingsActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //Toolbar modifications
        Toolbar my_toolbar = findViewById(R.id.mCustomToolbar);
        setSupportActionBar(my_toolbar);
        getSupportActionBar().setTitle("Inställningar");
        getSupportActionBar().setLogo(R.drawable.log);

        //Toogle modifications
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        NavigationView nv = findViewById(R.id.nav_menu);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem){
                //Switch send you to the activity that you click on by comparing ItemId med R.id.....
                switch(menuItem.getItemId()){
                    case(R.id.nav_mainmenu):
                        Intent sendToMainMenu = new Intent(getApplicationContext(),menuActivity.class);
                        startActivity(sendToMainMenu);
                        break;
                    case(R.id.nav_myprofile):
                        Intent sendToMyProfile = new Intent(getApplicationContext(),MyProfileActivity.class);
                        startActivity(sendToMyProfile);
                        break;
                    case(R.id.nav_logout):
                        Intent sendToLoginPage = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(sendToLoginPage);
                        break;
                    case(R.id.nav_settings):
                        Intent sendToSettingsPage = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(sendToSettingsPage);
                        break;
                    case(R.id.nav_allusers):
                        Intent sendToAllUsersPage = new Intent(getApplicationContext(), UsersActivity.class);
                        startActivity(sendToAllUsersPage);
                        break;
                    case(R.id.nav_addnewusers):
                        Intent sendToAddNewUsersPage = new Intent(getApplicationContext(), addContactActivity.class);
                        startActivity(sendToAddNewUsersPage);
                        break;
                }
                return true;
            }
        });

    }

    //Handle action bar item clicks here. The action bar will automatically handle clicks on the Home/Up button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

}