package com.example.rebeckagerdtham.skane18.activities;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.rebeckagerdtham.skane18.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;


public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        //ljud till knapp.
        //final MediaPlayer buttonclicksound = MediaPlayer.create(this, R.raw.buttonclick);


        //Toolbar modifications
        Toolbar my_toolbar = findViewById(R.id.mCustomToolbar);
        setSupportActionBar(my_toolbar);
        getSupportActionBar().setTitle("Habilitering och hjälpmedel");
        getSupportActionBar().setLogo(R.drawable.log);

        mAuth = FirebaseAuth.getInstance();
        final EditText mEmail = findViewById(R.id.usernameEditText);
        final EditText mPassword = findViewById(R.id.passwordEditText);

        //Vad som händer när vi trycker på "logga in"
        Button b =findViewById(R.id.loginButton);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //buttonclicksound.start();
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();

                if(!TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)){
                    loginUser(email, password);
                }else{
                    Toast.makeText(LoginActivity.this, "Minst ett av fälten är tomt", Toast.LENGTH_SHORT).show();
                }

            }
        });

        //Vad som händer när vi trycker på "skapa nytt konto"
        Button b1 = findViewById(R.id.newAccountButton);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //buttonclicksound.start();
                Intent registerpage =  new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(registerpage);

            }
        });

    }

    //Metod för att logga in
    private void loginUser(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Intent homepage = new Intent(LoginActivity.this, menuActivity.class);
                    startActivity(homepage);
                    finish();
                }else{
                    Toast.makeText(LoginActivity.this, "Inloggningen misslyckades. Kontrollera användarnamn och lösenord", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}


