package com.example.rebeckagerdtham.skane18.activities;


import android.content.ClipData;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableWrapper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.rebeckagerdtham.skane18.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Vector;

public class menuActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    private DatabaseReference mFriendRequestDatabase;
    private FirebaseUser mCurrent_user;

    private DatabaseReference mUserDatabase;
    private DatabaseReference mNotificationDatabase;
    private DatabaseReference mChatDatabase;

    private RecyclerView mChatList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //Toolbar modifications
        Toolbar my_toolbar = findViewById(R.id.mCustomToolbar);
        setSupportActionBar(my_toolbar);
        getSupportActionBar().setTitle("Mina konversationer");
        getSupportActionBar().setIcon(R.drawable.log);

        //Toogle modifications
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        NavigationView nv = findViewById(R.id.nav_menu);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem){
                //Switch send you to the activity that you click on by comparing ItemId med R.id.....
                switch(menuItem.getItemId()){
                    case(R.id.nav_mainmenu):
                        Intent sendToMainMenu = new Intent(getApplicationContext(),menuActivity.class);
                        startActivity(sendToMainMenu);
                        break;
                    case(R.id.nav_myprofile):
                        Intent sendToMyProfile = new Intent(getApplicationContext(),MyProfileActivity.class);
                        startActivity(sendToMyProfile);
                        break;
                    case(R.id.nav_logout):
                        Intent sendToLoginPage = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(sendToLoginPage);
                        break;
                    case(R.id.nav_settings):
                        Intent sendToSettingsPage = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(sendToSettingsPage);
                        break;
                    case(R.id.nav_allusers):
                        Intent sendToAllUsersPage = new Intent(getApplicationContext(), UsersActivity.class);
                        startActivity(sendToAllUsersPage);
                        break;
                    case(R.id.nav_addnewusers):
                        Intent sendToAddNewUsersPage = new Intent(getApplicationContext(), addContactActivity.class);
                        startActivity(sendToAddNewUsersPage);
                        break;
                }
                return true;
            }
        });

        ImageButton quickAddContact = findViewById(R.id.searchImageButton);
        quickAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendToAddContactActivity =  new Intent(getApplicationContext(), addContactActivity.class);
                startActivity(sendToAddContactActivity);
            }
        });
    }

    //Handle action bar item clicks here. The action bar will automatically handle clicks on the Home/Up button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }














        //Hanterar notifikationer
        int id = item.getItemId();
        if(id == R.id.action_item_one){
            mNotificationDatabase = FirebaseDatabase.getInstance().getReference().child("Notifications").child(mCurrent_user.getUid());
            mNotificationDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final String user_id_sent = dataSnapshot.child("id").getValue().toString();
                    mUserDatabase = FirebaseDatabase.getInstance().getReference().child("users");
                    mUserDatabase.child(user_id_sent).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String user_name_sent = dataSnapshot.child("name").getValue().toString();
                            String user_status_sent = dataSnapshot.child("status").getValue().toString();
                            Intent sendToSelectedUserProfile = new Intent(menuActivity.this, TheOtherUsersProfileActivity.class);
                            sendToSelectedUserProfile.putExtra("user_id", user_id_sent);
                            sendToSelectedUserProfile.putExtra("name", user_name_sent);
                            sendToSelectedUserProfile.putExtra("status", user_status_sent);
                            startActivity(sendToSelectedUserProfile);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Lägger till notifikationen uppe i högra hörnet när man har en kontaktförfrågan
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        mFriendRequestDatabase = FirebaseDatabase.getInstance().getReference().child("Friend_req");
        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
        String current_user_id = mCurrent_user.getUid();
        mFriendRequestDatabase.child(current_user_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("recieved")){
                    getMenuInflater().inflate(R.menu.toolbar_menu, menu);
                    TextView new_event = findViewById(R.id.new_event);
                    new_event.setText("Ny händelse!");
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return true;
    }













   //Hanterar tidigare konversationer

    @Override
    protected void onStart() {
        super.onStart();

        final TextView info = findViewById(R.id.infoIfYouHaveNoContactas);

        //Sätter upp tidigare konversationer
        mChatList = findViewById(R.id.chat_list);
        mChatList.setHasFixedSize(true);
        mChatList.setLayoutManager(new LinearLayoutManager(this));

        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
        final String current_user_id = mCurrent_user.getUid();

        Query query = FirebaseDatabase.getInstance().getReference().child("Chat").child(current_user_id);
        FirebaseRecyclerOptions<Users> options = new FirebaseRecyclerOptions.Builder<Users>().setQuery(query, Users.class).build();
        FirebaseRecyclerAdapter<Users, UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, UsersViewHolder>(options) {

            @Override
            protected void onBindViewHolder(@NonNull final UsersViewHolder holder, int position, @NonNull final Users model) {

                final String user_id = getRef(position).getKey();

                mUserDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(user_id);
                mUserDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final String user_name = dataSnapshot.child("name").getValue().toString();
                        final String user_status = dataSnapshot.child("status").getValue().toString();

                        holder.setDisplayName(user_name);
                        holder.setUserStatus(user_status);

                        holder.mView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent sendToSelectedChat = new Intent(menuActivity.this, ChattActivity.class);
                                sendToSelectedChat.putExtra("user_id", user_id);
                                sendToSelectedChat.putExtra("name", user_name);
                                sendToSelectedChat.putExtra("status", user_status);
                                startActivity(sendToSelectedChat);
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }

            @NonNull
            @Override
            public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_user_layout, parent, false);

                return new UsersViewHolder(view);
            }
        };

        firebaseRecyclerAdapter.startListening();
        mChatList.setAdapter(firebaseRecyclerAdapter);

        //Om vi har tidigare konversationer så visas mChatList, annars visas infomeddelandet om att man inte har tidigare konversationer
        mChatDatabase = FirebaseDatabase.getInstance().getReference().child("Chat");
        mChatDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(current_user_id)) {
                    info.setVisibility(View.INVISIBLE);
                }else{
                    mChatList.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        //Denna metod sätter TextView (contact_name) till de användare som finns i databasen
        public void setDisplayName(String name){
            TextView userNameView = mView.findViewById(R.id.contact_name);
            userNameView.setText(name);

        }

        //Denna metod sätter TextView (contact_status) till rätt status för rätt användare
        public void setUserStatus(String status){
            TextView statusUserView = mView.findViewById(R.id.contact_status);
            statusUserView.setText(status);

        }
    }
}
        





      

      

      



