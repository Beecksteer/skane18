package com.example.rebeckagerdtham.skane18.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.rebeckagerdtham.skane18.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class addContactActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    private EditText mSearchField;
    private ImageButton mSearchButton;
    private RecyclerView mResultList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        //Toolbar modifications
        Toolbar my_toolbar = findViewById(R.id.mCustomToolbar);
        setSupportActionBar(my_toolbar);
        getSupportActionBar().setTitle("Lägg till nya kontakter");
        getSupportActionBar().setLogo(R.drawable.log);

        //Toogle modifications
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        NavigationView nv = findViewById(R.id.nav_menu);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem){
                //Switch send you to the activity that you click on by comparing ItemId med R.id.....
                switch(menuItem.getItemId()){
                    case(R.id.nav_mainmenu):
                        Intent sendToMainMenu = new Intent(getApplicationContext(),menuActivity.class);
                        startActivity(sendToMainMenu);
                        break;
                    case(R.id.nav_myprofile):
                        Intent sendToMyProfile = new Intent(getApplicationContext(),MyProfileActivity.class);
                        startActivity(sendToMyProfile);
                        break;
                    case(R.id.nav_logout):
                        Intent sendToLoginPage = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(sendToLoginPage);
                        break;
                    case(R.id.nav_settings):
                        Intent sendToSettingsPage = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(sendToSettingsPage);
                        break;
                    case(R.id.nav_allusers):
                        Intent sendToAllUsersPage = new Intent(getApplicationContext(), UsersActivity.class);
                        startActivity(sendToAllUsersPage);
                        break;
                    case(R.id.nav_addnewusers):
                        Intent sendToAddNewUsersPage = new Intent(getApplicationContext(), addContactActivity.class);
                        startActivity(sendToAddNewUsersPage);
                        break;
                }
                return true;
            }
        });

        mSearchField = findViewById(R.id.search_field);
        mSearchButton = findViewById(R.id.search_button);
        mResultList = findViewById(R.id.searchResult_list);
        mResultList.setHasFixedSize(true);
        mResultList.setLayoutManager(new LinearLayoutManager(this));

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchText = mSearchField.getText().toString();
                userSearch(searchText);

                //Stänger tangenbordet när man trycker på förstoringsglaset,(sökknappen).
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });



    }

    //Handle action bar item clicks here. The action bar will automatically handle clicks on the Home/Up button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private void userSearch(String searchText){

        //Sista delen, dvs. orderBytChild...osv, är själva sökfunktionen, \uf8ff är en uni som sköter den biten.
        Query query = FirebaseDatabase.getInstance().getReference().child("users").orderByChild("name").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerOptions<Users> options = new FirebaseRecyclerOptions.Builder<Users>().setQuery(query, Users.class).build();
        FirebaseRecyclerAdapter<Users, UserViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, UserViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull UserViewHolder holder, int position, @NonNull final Users model) {

                holder.setContactName(model.getName());
                holder.setContactStatus(model.getStatus());

                //Skickar användaren till den profil som klickas på
                final String user_id = getRef(position).getKey();
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent sendToSelectedUserProfile = new Intent(addContactActivity.this, TheOtherUsersProfileActivity.class);
                        sendToSelectedUserProfile.putExtra("user_id", user_id);
                        sendToSelectedUserProfile.putExtra("name", model.getName());
                        sendToSelectedUserProfile.putExtra("status", model.getStatus());
                        startActivity(sendToSelectedUserProfile);
                    }
                });
            }

            @NonNull
            @Override
            public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_user_layout, parent, false);

                return new UserViewHolder(view);
            }
        };

        //Sätter på firebaseRecyclerAdapter och kopplar den till mUserList som är vår recyclerview
        firebaseRecyclerAdapter.startListening();
        mResultList.setAdapter(firebaseRecyclerAdapter);

    }


    public static class UserViewHolder extends RecyclerView.ViewHolder{

        View mView;

        public UserViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        //Denna metod sätter TextView (contact_name) till de användare som finns i databasen
        public void setContactName(String name){
            TextView contactNameView = mView.findViewById(R.id.contact_name);
            contactNameView.setText(name);

        }

        //Denna metod sätter TextView (contact_status) till rätt status för rätt användare
        public void setContactStatus(String status){
            TextView contactStatusView = mView.findViewById(R.id.contact_status);
            contactStatusView.setText(status);

        }


    }

}
