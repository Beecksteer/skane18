package com.example.rebeckagerdtham.skane18.model;

import java.util.List;

/**
 * Created by RebeckaGerdtham on 2018-02-05.
 */

public class Chat {
    private String patientName;
    private String caretakerId;
    private List<ChatMessage> chatMessageList;

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getCaretakerId() {
        return caretakerId;
    }

    public void setCaretakerId(String caretakerId) {
        this.caretakerId = caretakerId;
    }

    public List<ChatMessage> getChatMessageList() {
        return chatMessageList;
    }

    public void setChatMessageList(List<ChatMessage> chatMessageList) {
        this.chatMessageList = chatMessageList;
    }


    public void writeMessage(String owner, String message){
        ChatMessage chatMessage = new ChatMessage(owner,message);
        this.chatMessageList.add(chatMessage);
    }
}
