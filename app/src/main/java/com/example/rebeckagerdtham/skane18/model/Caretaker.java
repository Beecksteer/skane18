package com.example.rebeckagerdtham.skane18.model;

/**
 * Created by RebeckaGerdtham on 2018-02-05.
 */

public class Caretaker {
    private String caretakerId;
    private String firstname;
    private String password;

    public String getCaretakerId() {
        return caretakerId;
    }

    public void setCaretakerId(String caretakerId) {
        this.caretakerId = caretakerId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPassword() {
        return password;
    }

    public void setCaretakerPassword(String password) {
        this.password = password;
    }





}
