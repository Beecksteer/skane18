package com.example.rebeckagerdtham.skane18.activities;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rebeckagerdtham.skane18.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {

    private List<Messages> mMessageList;
    private FirebaseAuth mAuth;

    public MessageAdapter(List<Messages> mMessageList){
        this.mMessageList = mMessageList;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_single_layout, parent, false);

        return new MessageViewHolder(v);
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView messageText;
        public ImageView profileImage;
        public TextView messageText2;

        public MessageViewHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.message_text_layout_left);
            profileImage = itemView.findViewById(R.id.message_profile_layout);
            messageText2 = itemView.findViewById(R.id.message_text_layout_right);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {

        mAuth = FirebaseAuth.getInstance();
        String current_user_id = mAuth.getCurrentUser().getUid();

        Messages c = mMessageList.get(position);

        String from_user = c.getFrom();

        //Om vi skickar meddelandet
        if(from_user.equals(current_user_id)){

            holder.messageText2.setBackgroundResource(R.drawable.message_text_background_2);
            holder.messageText2.setTextColor(Color.WHITE);
            holder.messageText2.setTextSize(20);
            holder.messageText2.setText(c.getMessage());
            holder.messageText.setVisibility(View.GONE);
            holder.profileImage.setVisibility(View.GONE);

            //Om den andra personen skickar meddelandet
        }else{

            holder.messageText.setBackgroundResource(R.drawable.message_text_background);
            holder.messageText.setTextColor(Color.WHITE);
            holder.messageText.setTextSize(20);
            holder.messageText.setText(c.getMessage());
            holder.messageText2.setVisibility(View.GONE);

        }


    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


}
