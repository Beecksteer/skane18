package com.example.rebeckagerdtham.skane18.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rebeckagerdtham.skane18.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;

public class TheOtherUsersProfileActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    private DatabaseReference mUsersDatabase;
    private DatabaseReference mFriendRequestDatabase;
    private DatabaseReference mFriendDatabase;
    private DatabaseReference mNotificationDatabase;

    private FirebaseUser mCurrent_user;
    private String mCurrent_state;

    private Button mSendFriendRequestBtn;
    private Button mDeclineFriendRequestBtn;

    HashMap<String, String> user_ID_Map = new HashMap<>();
    HashMap<String, String> current_user_Map = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_other_users_profile);

        //Toolbar modifications
        Toolbar my_toolbar = findViewById(R.id.mCustomToolbar);
        setSupportActionBar(my_toolbar);
        getSupportActionBar().setLogo(R.drawable.log);

        //Toogle modifications
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        NavigationView nv = findViewById(R.id.nav_menu);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem){
                //Switch send you to the activity that you click on by comparing ItemId med R.id.....
                switch(menuItem.getItemId()){
                    case(R.id.nav_mainmenu):
                        Intent sendToMainMenu = new Intent(getApplicationContext(),menuActivity.class);
                        startActivity(sendToMainMenu);
                        break;
                    case(R.id.nav_myprofile):
                        Intent sendToMyProfile = new Intent(getApplicationContext(),MyProfileActivity.class);
                        startActivity(sendToMyProfile);
                        break;
                    case(R.id.nav_logout):
                        Intent sendToLoginPage = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(sendToLoginPage);
                        break;
                    case(R.id.nav_settings):
                        Intent sendToSettingsPage = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(sendToSettingsPage);
                        break;
                    case(R.id.nav_allusers):
                        Intent sendToAllUsersPage = new Intent(getApplicationContext(), UsersActivity.class);
                        startActivity(sendToAllUsersPage);
                        break;
                    case(R.id.nav_addnewusers):
                        Intent sendToAddNewUsersPage = new Intent(getApplicationContext(), addContactActivity.class);
                        startActivity(sendToAddNewUsersPage);
                        break;
                }
                return true;
            }
        });

        //Hämtar den information vi skickade med intenten i UsersActivity
        final String user_id =getIntent().getStringExtra("user_id");
        final String user_id_name = getIntent().getStringExtra("name");
        final String user_id_status = getIntent().getStringExtra("status");
        user_ID_Map.put("name", user_id_name);
        user_ID_Map.put("status", user_id_status);

        mCurrent_state = "not_friends";
        mFriendRequestDatabase = FirebaseDatabase.getInstance().getReference().child("Friend_req");
        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();

        mSendFriendRequestBtn = findViewById(R.id.addContactButton);
        mDeclineFriendRequestBtn = findViewById(R.id.declineRequestButton);
        mDeclineFriendRequestBtn.setVisibility(View.INVISIBLE);

        mFriendDatabase = FirebaseDatabase.getInstance().getReference().child("Friends");


        //Sätter upp profilinformationen
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(user_id);
        mUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("name").getValue().toString();
                TextView display_name = findViewById(R.id.displayUserName);
                display_name.setText(name);
                String status = dataSnapshot.child("status").getValue().toString();
                TextView display_status = findViewById(R.id.displayUserStatus);
                display_status.setText(status);

                //Sätter toolbartiteln till profilägarens namn
                getSupportActionBar().setTitle(name);

                // - -------- FRIENDS LIST / REQUEST FEATURE -------- -
                mFriendRequestDatabase.child(mCurrent_user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(dataSnapshot.child("recieved").hasChild(user_id)){

                                mCurrent_state = "req_received";
                                mSendFriendRequestBtn.setText("Acceptera kontaktförfrågan");
                                mDeclineFriendRequestBtn.setVisibility(View.VISIBLE);

                                //Avböj vänfrågan, (senare tillstånd)
                                mDeclineFriendRequestBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mFriendRequestDatabase.child(mCurrent_user.getUid()).child("recieved").child(user_id).removeValue()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                mFriendRequestDatabase.child(user_id).child("sent").child(mCurrent_user.getUid()).removeValue()
                                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        mDeclineFriendRequestBtn.setVisibility(View.INVISIBLE);
                                                        mCurrent_state = "not_friends";
                                                        mSendFriendRequestBtn.setText("Skicka en kontaktförfrågan");
                                                        mSendFriendRequestBtn.setBackgroundResource(R.drawable.chattbutton);

                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                            }
                            if(dataSnapshot.child("sent").hasChild(user_id)){
                                mSendFriendRequestBtn.setText("Avbryt kontaktförfrågan");
                                mSendFriendRequestBtn.setBackgroundResource(R.drawable.standardbutton);
                                mCurrent_state="req_sent";
                            } else{
                            mFriendDatabase.child(mCurrent_user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    if(dataSnapshot.hasChild(user_id)){

                                        mCurrent_state = "friends";
                                        mDeclineFriendRequestBtn.setVisibility(View.VISIBLE);
                                        mSendFriendRequestBtn.setText("Starta en konversation");
                                        mDeclineFriendRequestBtn.setText("Ta bort kontakt");

                                        //Startar en konversation, (senare tillstånd)
                                        mSendFriendRequestBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent send_to_chatt = new Intent(getApplicationContext(), ChattActivity.class);
                                                send_to_chatt.putExtra("user_id", user_id);
                                                send_to_chatt.putExtra("name", user_id_name);
                                                send_to_chatt.putExtra("status", user_id_status);
                                                startActivity(send_to_chatt);
                                            }
                                        });

                                        //Ta bort vän, (senare tillstånd)
                                        mDeclineFriendRequestBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mFriendDatabase.child(mCurrent_user.getUid()).child(user_id).removeValue()
                                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {
                                                                mFriendDatabase.child(user_id).child(mCurrent_user.getUid()).removeValue()
                                                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                            @Override
                                                                            public void onSuccess(Void aVoid) {
                                                                                mDeclineFriendRequestBtn.setVisibility(View.INVISIBLE);
                                                                                mCurrent_state = "not_friends";
                                                                                mSendFriendRequestBtn.setText("Skicka en kontaktförfrågan");
                                                                            }
                                                                        });
                                                            }
                                                        });
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Vad som händer när man klicka på "Skicka vänförfrågan till någon man inte är vän med
        mSendFriendRequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSendFriendRequestBtn.setEnabled(false);

                // - --------NOT FRIENDS STATE------------ -
                if(mCurrent_state.equals("not_friends")){
                    mFriendRequestDatabase.child(mCurrent_user.getUid()).child("sent").child(user_id)
                            .setValue("true").addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                mFriendRequestDatabase.child(user_id).child("recieved").child(mCurrent_user.getUid())
                                        .setValue("true").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                        mNotificationDatabase = FirebaseDatabase.getInstance().getReference().child("Notifications");
                                        mNotificationDatabase.child(user_id).child("id").setValue(mCurrent_user.getUid());


                                        //När man tryckt på "Friend Request Button" så kommer den bli avvaktiverad så att man inte kan göra det igen.
                                        mSendFriendRequestBtn.setEnabled(true);

                                        mCurrent_state = "req_sent";
                                        mSendFriendRequestBtn.setBackgroundResource(R.drawable.standardbutton);
                                        mSendFriendRequestBtn.setText("Avbryt kontaktförfrågan");

                                    }
                                });

                            }else{
                                Toast.makeText(TheOtherUsersProfileActivity.this, "Failed sending request", Toast.LENGTH_SHORT).show();
                            }
                        }

                    });

                }

                // - --------CANCEL REQUEST STATE------------ -
                if(mCurrent_state.equals("req_sent")){
                    mFriendRequestDatabase.child(mCurrent_user.getUid()).child("sent").child(user_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            mFriendRequestDatabase.child(user_id).child("recieved").child(mCurrent_user.getUid()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                    mNotificationDatabase = FirebaseDatabase.getInstance().getReference().child("Notifications");
                                    mNotificationDatabase.child(user_id).removeValue();

                                    mSendFriendRequestBtn.setEnabled(true);

                                    mCurrent_state = "not_friends";
                                    mSendFriendRequestBtn.setBackgroundResource(R.drawable.chattbutton);
                                    mSendFriendRequestBtn.setText("Skicka en kontaktförfrågan");

                                }
                            });

                        }
                    });
                }

                // - --------REQUEST RECEIVED STATE------------ -
                if(mCurrent_state.equals("req_received")){

                    mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(mCurrent_user.getUid());
                    mUsersDatabase.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String current_user_name = dataSnapshot.child("name").getValue().toString();
                            String current_user_status = dataSnapshot.child("status").getValue().toString();
                            current_user_Map.put("name", current_user_name);
                            current_user_Map.put("status", current_user_status);

                            final String currentDate = DateFormat.getDateTimeInstance().format(new Date());

                            mFriendDatabase.child(mCurrent_user.getUid()).child(user_id).setValue(user_ID_Map)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {

                                            mFriendDatabase.child(user_id).child(mCurrent_user.getUid()).setValue(current_user_Map)
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {

                                                            mFriendRequestDatabase.child(mCurrent_user.getUid()).child("recieved").child(user_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                @Override
                                                                public void onSuccess(Void aVoid) {

                                                                    mFriendRequestDatabase.child(user_id).child("sent").child(mCurrent_user.getUid()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                        @Override
                                                                        public void onSuccess(Void aVoid) {

                                                                            mSendFriendRequestBtn.setEnabled(true);

                                                                            mCurrent_state = "friends";

                                                                            mSendFriendRequestBtn.setText("Starta en konversation");
                                                                            mDeclineFriendRequestBtn.setText("Ta bort kontakt");

                                                                            mNotificationDatabase = FirebaseDatabase.getInstance().getReference().child("Notifications");
                                                                            mNotificationDatabase.child(user_id).removeValue();

                                                                            //Vad som händer om man trycker på "Starta en konversation"
                                                                            mSendFriendRequestBtn.setOnClickListener(new View.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(View v) {
                                                                                    Intent send_to_chatt = new Intent(getApplicationContext(), ChattActivity.class);
                                                                                    send_to_chatt.putExtra("user_id", user_id);
                                                                                    send_to_chatt.putExtra("name", user_id_name);
                                                                                    send_to_chatt.putExtra("status", user_id_status);
                                                                                    startActivity(send_to_chatt);
                                                                                }
                                                                            });

                                                                            //Ta bort vän i realtid
                                                                            mDeclineFriendRequestBtn.setOnClickListener(new View.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(View v) {
                                                                                    mFriendDatabase.child(mCurrent_user.getUid()).child(user_id).removeValue()
                                                                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                                                @Override
                                                                                                public void onSuccess(Void aVoid) {
                                                                                                    mFriendDatabase.child(user_id).child(mCurrent_user.getUid()).removeValue()
                                                                                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                                                                @Override
                                                                                                                public void onSuccess(Void aVoid) {
                                                                                                                    mDeclineFriendRequestBtn.setVisibility(View.INVISIBLE);
                                                                                                                    mCurrent_state = "not_friends";
                                                                                                                    mSendFriendRequestBtn.setText("Skicka en kontaktförfrågan");
                                                                                                                }
                                                                                                            });
                                                                                                }
                                                                                            });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                        }
                                    });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                }
            }
        });
    }

    //Handle action bar item clicks here. The action bar will automatically handle clicks on the Home/Up button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

}
